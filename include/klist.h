/* vcc/include/klist.h 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __KLIST_H
#define __KLIST_H

#include <vcc/vcc.h>
#include <stddef.h>


/* before, i think using a 'container_of' in things we did in kernel 
 * is silly, but now i realize that, i'm the stupid one, there are 
 * too many things in the structure. */

struct klist_node {
	struct klist_node 		*next, *prev;
};



void klist_init(struct klist_node *head);

void klist_add(struct klist_node *head, struct klist_node *new);
void klist_del(struct klist_node *head, struct klist_node *will_del);

#define klist_empty(list) ((list)->next == (list))

#define KLIST_NODE_INIT(__x) { \
	.next = __x, \
	.prev = __x, \
}

#define container_of(ptr, type, member) ((type *) ((void *) (ptr) - offsetof(type, member)))

#endif

