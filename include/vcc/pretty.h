/* vccd/include/vcc/pretty.h 
 * 
 * This file is part of vcc.  
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __VCC_PRETTY_H
#define __VCC_PRETTY_H

#include <stdio.h>

#define FG_BLACK 		30
#define FG_RED 			31
#define FG_GREEN 		32
#define FG_YELLOW 		33
#define FG_BLUE 		34
#define FG_MAGENTA 		35
#define FG_LIGHTBLUE 		36
#define FG_WHITE 		37
#define FG_GRAY 		90

#define BG_BLACK 		40
#define BG_RED 			41
#define BG_GREEN 		42
#define BG_YELLOW 		43
#define BG_BLUE 		44
#define BG_MAGENTA 		45
#define BG_LIGHTBLUE 		46
#define BG_WHITE 		47
#define BG_GRAY 		100

#define MODE_DEFAULT 		0
#define MODE_HIGHLIGHT 		1
#define MODE_LINE 		4
#define MODE_BLINK 		5

#define ansi_csi_st(strm, m, fg, bg) 	\
	fprintf(strm, "\033[%d;%d;%dm", m, fg, bg)

#define ansi_csi_ed(strm) 		\
	fprintf(strm, "\033[0m")

#define setcolor(strm, c) 		\
	ansi_csi_st(strm, (c)->mode, (c)->fg, (c)->bg)

#define MSG_NEW_RELAY 		1
#define MSG_NEW_ONLY_VISIBLE 	2
#define MSG_ENCRYPTED 		4

struct color {
	int 		fg;
	int 		bg;
	int 		mode;
};


struct theme {
	struct color 	usrname, 
			sysusername, 
			msg, 
			sysmsg, 
			sid, 
			lvl, 
			time, 
			relay, 
			errmsg, 
			helpcmd, 
			helpmsg, 
			cqd;
};

#define NR_THEMES 	12


int pretty_new_msg(char *usrname, char *msg, int sid, int flags);
int pretty_prompt(char *usrname);
int pretty_unknown_cmd(char *cmd);
int pretty_cmd_help(char *cmd, char *msg);
int pretty_cqd(char *usrname, char *msg);


#endif

