/* vcc/include/vcc/hooks.h 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __VCC_HOOKS_H
#define __VCC_HOOKS_H

#include <sys/types.h>
#include <vcc/vcc.h>
#include <vcc/hooks.h>
#include <klist.h>
#include <libintl.h>


#define HOOK_RECV 	1
#define HOOK_SEND 	2

struct hook_info {
	int 			type;
	char 			*msg;
	char 			*usr;
	int 			sid;
};

typedef char *(* hookfn_t) (struct hook_info *);

struct hook {
	int 			type;
	hookfn_t 		hook;
	struct klist_node 	node;
};

#define hook_of(n) 		container_of(n, struct hook, node)
/* failed, abore */
#define HOOK_FAILED 		NULL
/* ignored, continue */
#define HOOK_IGNORE 		((char *) 1)
/* has handled, so break hook calling */
#define HOOK_HANDLED 		((char *) 2)

struct hook *register_hook(int type, hookfn_t hook);
int do_hook_recv(char *s, char *usrname, int sid);
int do_hook_send(char *s);

#endif

