/* vcc/include/vcc/plugin.h 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __VCC_PLUGIN_H
#define __VCC_PLUGIN_H

#include <sys/types.h>
#include <vcc/vcc.h>
#include <vcc/hooks.h>
#include <libintl.h>

/* a plugin can define their own commands in cmd.c, and must check 
 * it inserted, and abort command execution if it doesn't. */


struct cmdtable {
	char 			*cmd;
	int 			(* do_cmd) (int argc, char **argv);
	char 			*help;
};

struct plugin {
#define PLG_INSERTED 		1
	int 			state;
	/* name of the plugin */
	char 			*name;
	/* short description */
	char 			*descr;
	int 			(* init) (void);
	int 			(* exit) (void);

	struct cmdtable 	*cmdtab;
};

/* plugin.c */
extern struct plugin 		*plugins[];

#define _(s) 			gettext(s)

#define plg_check_enabled() ({	\
	if (this_plg->state != PLG_INSERTED) { \
		fprintf(stderr, _("*** this plugin didn't insert. " \
				"please insert it by -pins and try it again. \n")); \
		return 1; \
	} \
})

int plg_insert(struct plugin *);
struct plugin *find_plg(char *name);

#endif

