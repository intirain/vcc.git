/* vcc/include/vcc/interfaces.h 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __VCC_INTERFACES_H
#define __VCC_INTERFACES_H

#include <sys/types.h>
#include <vcc/vcc.h>
#include <klist.h>

struct banlist {
	char 			*usrname;
	struct klist_node 	node;
};

#define banlist_of(n) 		container_of(n, struct banlist, node)

struct history {
	char 			*usrname;
	char 			*msg;
	struct klist_node 	node;
};

#define hist_of(n) 		container_of(n, struct history, node)

struct sname_cache {
	int 			sid;
	char 			name[USRNAME_SIZE];
	struct klist_node 	node;
};

#define sname_cache_of(n) 	container_of(n, struct sname_cache, node)

int vcc_login(char *, char *);
int do_intiauth(void);

int send_msg(const char *msg, const char *usrname);
int send_msg_encrypted(const char *msg, const char *s);
int send_relay_msg(const char *, const char *, const char *);
int read_msg(struct vcc_request *buf);
char *relay_get_msg(int fd, struct vcc_relay_header *);
int do_new_request(struct vcc_relay_header *);

int is_user_banned(char *);
int pretty_init(void);

int show_algo_info(void);
int crypt_init(void *key, void *iv);
int encrypt(char *);
int decrypt(char *);

int do_cmd(char *s);
int do_bh(struct vcc_request *);
int do_relay_bh(struct vcc_relay_header *);
int do_cmd_lself(int, char **);

int init_socket(const char *server_addr, int server_port);
int handle_command(char *);
int do_handle_command(char *);

int get_user_info(char *usrname);
int get_users(void);
int incr_score(char *username, int incr);
int vcc_create_session(char *name);
int get_sessions(void);
int join_session(int sid);
int quit_session(int sid);
int get_sys_info(void);
int session_name(int, char *);
char *getsname(int sid);
int get_name_sid(char *name);

int robot_new_msg(struct vcc_request *);

int vcc_login_bh(struct vcc_request *);
int sename_bh(struct vcc_request *);
int do_cmd_ls_bh(struct vcc_relay_header *);
int do_cmd_lsse_bh(struct vcc_request *);
int do_cmd_uinfo_bh(struct vcc_request *);
int do_cmd_incr_bh(struct vcc_request *);

struct history *hist_getmsg(int last);

extern int fd;
extern struct klist_node banned_people;
extern char usrname[];
extern int rate;
extern int current_sid;
extern int self_level;
extern int crypt_disabled;

#endif

