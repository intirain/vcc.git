/* vcc/include/vcc/vcc.h 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __VCC_VCC_H
#define __VCC_VCC_H

/* magic number in vcc_request to check that it's really a vcc package */
#define VCC_MAGIC 	0x01328e22
#define VCC_MAGIC_RL 	0x01328e36

/* the port that vccd is listening on */
#define VCC_PORT 	46

/* client -> server, send a message */
#define REQ_MSG_SEND 	1

/* server -> client. sent when received a message */
#define REQ_MSG_NEW 	2

/* client -> server. get users logged-in */
#define REQ_CTL_USRS 	3

/* client -> server. login with password and username.
 * server -> client. login ack  */
#define REQ_CTL_LOGIN 	4

/* client -> server. create a new session */
#define REQ_CTL_NEWSE 	5

/* client -> server: request (get sessions present). 
 * server -> client: response */
#define REQ_CTL_SESS 	6

/* client -> server. join session */
#define REQ_CTL_JOINS 	7

/* client -> server. get user info */
#define REQ_CTL_UINFO 	8

/* client -> server. increase one's score */
#define REQ_SYS_SCRINC 	9

/* client -> server. send new message. */
#define REQ_REL_MSG 	10

/* client -> server. intiauth login */
#define REQ_CTL_IALOG 	11

/* server -> client. new message */
#define REQ_REL_NEW 	12

/* client -> server. get server info (root). */
#define REQ_SYS_INFO 	13
#define REQ_CTL_SENAME 	14

/* client -> server. quit session */
#define REQ_CTL_QUITS	15

#define REQ_MAX 	15

#define REQ_SIZE 	512
#define USRNAME_SIZE 	32

/* we'll not warn you anyway.  */
#define PASSWD_SIZE 	64
#define MSG_SIZE 	(REQ_SIZE - 5 * sizeof(int) - USRNAME_SIZE)

#define FLAG_ENCRYPTED 	1

struct vcc_request {
	int 		magic;
	int 		reqtype;

	/* to make server easy and fast */
	int 		uid;
	int 		session;
	int 		flags;

	char 		usrname[USRNAME_SIZE];
	char 		msg[MSG_SIZE];
};


struct vcc_relay_header {
	int 		magic;
	int 		reqtype;
	int 		size;

	int 		uid;
	int 		session;

	char 		usrname[USRNAME_SIZE];
	char 		visible[USRNAME_SIZE];
};

#define to_boolean(c) 	(!!(c))
#define likely(c) 	__builtin_expect(to_boolean(c), 1)
#define unlikely(c) 	__builtin_expect(to_boolean(c), 0)

#endif

