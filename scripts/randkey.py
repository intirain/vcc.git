#!/bin/python3

# vcc/scripts/randkey.py 
# 
# This file is part of vcc. 
# 
# vcc is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
# 
# vcc is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
# GNU General Public License for more details. 
# 
# You should have received a copy of the GNU General Public License 
# along with vcc. If not, see <https://www.gnu.org/licenses/>




import argparse
import random

parser = argparse.ArgumentParser(description="Create random key", prog="randkey")
parser.add_argument("-s", "--block-size", type=int, default=32, help="Specified the size of a block")
args = parser.parse_args()

table = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
key = ""
iv = ""

for i in range(args.block_size):
    key += random.choice(table)
    iv += random.choice(table)

print("Key: %s\nIV: %s" % (key, iv))

