#!/bin/python3

# vcc/scripts/mktheme.py 
# 
# This file is part of vcc. 
# 
# vcc is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
# 
# vcc is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
# GNU General Public License for more details. 
# 
# You should have received a copy of the GNU General Public License 
# along with vcc. If not, see <https://www.gnu.org/licenses/>




import os
import sys
from random import choice

random = False


modes = {'def': 0, 'high': 1, 'under': 4, 'blink': 5}
fgs = {'black': 30, 'red': 31, 'green': 32, 'yellow': 33, 'blue': 34, \
        'magenta': 35, 'lightblue': 36, 'white': 37, 'gray': 90}
bgs = {'black': 40, 'red': '41', 'green': 42, 'yellow': 43, 'blue': 44, \
        'magenta': 45, 'lightblue': 46, 'white': 47, 'gray': 100}

fd = open("{}/.vcc-theme".format(os.getenv("HOME")), "w")

if len(sys.argv) == 2 and sys.argv[1] == '--random':
    random = True

else:
    print("modes: ", modes)
    print("fgs: ", fgs)
    print("bgs: ", bgs)

def put(s, n, d):
    fd.write("seti {s}_{n} = {d}\n".format(s=s, n=n, d=d))

def get(s):
    l = {'mode': modes, 'fg': fgs, 'bg': bgs}

    for k, v in l.items():
        if random:
            put(s, k, choice(list(v.values())))
            continue

        while True:
            print("[%s] %s: " % (s, k), end='')

            try:
                n = input()
                put(s, k, v[n])

                break

            except KeyboardInterrupt:
                print('')

                continue

            except KeyError:
                if n == 'quit':
                    exit(1)

                print("Invalid input. ")
                continue

entries = ['usrname', 'sysusername', 'msg', 'sysmsg', 'sid', 'lvl', 'time', \
        'relay', 'errmsg', 'helpcmd', 'helpmsg', 'cqd']

for i in entries:
    get(i)

