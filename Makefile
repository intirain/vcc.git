VCC_MAJOR 		= 1
VCC_MINOR 		= 37
VCC_PATCH 		= 3
VCC_STR 		= -rc

# change this to '/usr/share/' or '/opt/vcc/' etc
INSTALL 		= $(shell pwd)
MO_DIR 			= $(INSTALL)/lang/zh_CN/LC_MESSAGES

VCC_COMPILE_TIME 	= $(shell date)
VCC_COMPILE_BY 		= $(shell whoami)
VCC_SEMVER 		= vcc $(VCC_MAJOR).$(VCC_MINOR).$(VCC_PATCH)$(VCC_STR)
VCC_SEMVER_NS 		= vcc-$(VCC_MAJOR).$(VCC_MINOR).$(VCC_PATCH)$(VCC_STR)

macro_srcs 		= gnr_theme_macros.m4 gnr_load_themes.m4 
macro_objs 		= gnr_theme_macros.s gnr_load_themes.s 
srcs 			= main.c vcc.c pretty.c cmd.c crypt.c klist.c plugins.c robot.c intiauth.c hooks.c 
objs 			= main.o vcc.o pretty.o cmd.o crypt.o klist.o plugins.o robot.o intiauth.o hooks.o 
target 			= vcc 
libgcrypt_cflags 	= $(shell libgcrypt-config --libs)
CFLAGS 			= -std=gnu11 -Wall -Wextra -Werror -Iinclude -g $(libgcrypt_cflags) -Lplugins -lplugins \
			  -Llibs -lreadconf -Ilibs/readconf/include 
MAKEFLAGS 		= --no-print-directory
VCC_DEFSERV 		= 39.104.27.216
VCC_VERSION 		= vcc $(VCC_MAJOR).$(VCC_MINOR).$(VCC_PATCH) \
		  $(VCC_COMPILE_TIME) $(VCC_COMPILE_BY)

all : macros
	@echo "$(VCC_VERSION)"
	@echo "$(VCC_SEMVER)"
	@echo "#define VCC_VERSION \"$(VCC_VERSION)\"" > include/vcc/version.h 
	@echo "#define VCC_SEMVER \"$(VCC_SEMVER)\"" >> include/vcc/version.h 
	@echo "#define VCC_DEFSERV \"$(VCC_DEFSERV)\"" > include/vcc/defserv.h 
	@echo "#define INSTALL_PATH \"$(INSTALL)\"" > include/vcc/config.h 
	@$(MAKE) -C libs/ 
	@$(MAKE) -C plugins/ 
	@$(MAKE) $(target) $(MAKEFLAGS)
	@$(MAKE) mo

macros : $(macro_srcs) $(macro_objs)
%.s : %.m4  
	@m4 $*.m4 > $*.s 
	@echo -e "M4\t\t\t$*.m4"

mo : 
	@mkdir -p $(MO_DIR)
	@msgfmt --check --verbose --output-file $(MO_DIR)/vcc.mo po/zh_CN.po 


$(target) : 	$(objs) $(srcs)
	@$(CC) $(objs) $(CFLAGS) -o $(target)
	@echo -e "LD\t\t\t$(target)"

%.o : %.c
	@$(CC) -c $(CFLAGS) $*.c 
	@echo -e "CC\t\t\t$*.c"

backup : clean
	(dir=$$(basename `pwd`); cd ..; tar cf $(VCC_SEMVER_NS).tar.gz $$dir/*)

.PHONY : clean
clean :
	$(RM) $(objs) $(target) $(macro_objs) -f 
	$(RM) -rf lang 
	@$(MAKE) -C libs/ clean
	@$(MAKE) -C plugins/ clean

