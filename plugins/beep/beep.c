/* vcc/plugins/beep/beep.c 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <vcc/vcc.h>
#include <vcc/pretty.h>
#include <vcc/plugin.h>
#include <vcc/version.h>


int beep_init(void);

struct cmdtable beep_cmdtab[] = {
	{ NULL, NULL, NULL }
};

struct plugin plg_beep = {
	.state = 0, 
	.name = "beep", 
	.descr = "Beep when you have a new message. ", 
	.init = beep_init, 
	.cmdtab = beep_cmdtab
};

#define this_plg (&plg_beep)

char *beep_hook(struct hook_info *hi) {
	char buf[128];
	char *p;
	int len;

	if (unlikely(!(p = strdup(hi->msg)))) {
		fprintf(stderr, "*** malloc() failed\n");

		return HOOK_FAILED;
	}

	/* '\n' */
	len = strlen(p);
	p[len - 1] = 0;

	if (len > 10) {
		p[10] = 0;
		strcat(p, "...");
	}

	sprintf(buf, "notify-send \"vcc\" \"vcc: %s: %s\"", hi->usr, p);
	printf("%s\n", buf);

	fprintf(stderr, "\a");
	system(buf);
	free(p);

	return HOOK_IGNORE;
}

int beep_init(void) {
	register_hook(HOOK_RECV, beep_hook);
	return 0;
}


