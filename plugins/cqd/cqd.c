/* vcc/plugins/cqd/cqd.c 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <stdio.h>
#include <string.h>

#include <vcc/vcc.h>
#include <vcc/pretty.h>
#include <vcc/plugin.h>
#include <vcc/version.h>
#include <vcc/interfaces.h>


int do_cmd_cqd(int argc, char **argv);
int cqd_init(void);

struct cmdtable cqd_cmdtab[] = {
	{ "-cqd", do_cmd_cqd, "Raise CQD. " }, 

	{ NULL, NULL, NULL }
};

struct plugin plg_cqd = {
	.state = 0, 
	.name = "cqd", 
	.descr = "Raise CQD. ", 
	.init = cqd_init, 
	.cmdtab = cqd_cmdtab
};

#define this_plg (&plg_cqd)


int do_cmd_cqd(int argc, char **argv) {
	char buf[MSG_SIZE];
	char *p;

	(void) argv;

	plg_check_enabled();

	switch (argc) {
	case 1:
		p = "CQD\n";
		break;

	case 2:
		p = argv[1];
		break;

	default:
		fprintf(stderr, _("usage: -cqd\n"));
		return 1;
	}

	sprintf(buf, "-cqd#%s\n", p);
	send_msg(buf, usrname);

	return 0;
}


char *cqd_hook(struct hook_info *hi) {
	if (strncmp(hi->msg, "-cqd#", 5)) 
		return HOOK_IGNORE;

	pretty_cqd(hi->usr, hi->msg + 5);
	return HOOK_HANDLED;
}


int cqd_init(void) {
	register_hook(HOOK_RECV, cqd_hook);
	return 0;
}


