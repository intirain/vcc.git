/* vcc/plugins/robot/robot.c 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

#include <vcc/vcc.h>
#include <vcc/pretty.h>
#include <vcc/plugin.h>
#include <vcc/version.h>
#include <vcc/interfaces.h>


int robot_lslcn(char *s);
int do_robot_cmd(char *s);

struct cmdtable robot_cmdtab[] = {
	{ NULL, NULL, NULL }
};

struct plugin plg_robot = {
	.state = 0, 
	.name = "robot", 
	.descr = "The robot plugin. ", 
	.cmdtab = robot_cmdtab
};

static struct cmdtable commands[] = {
	{ NULL, NULL, NULL }
};

#define this_plg (&plg_robot)


int robot_new_msg(struct vcc_request *req) {
	char *msg;

	if (ntohl(req->reqtype) != REQ_MSG_NEW) 
		return do_bh(req);

	msg = req->msg;

	if (strncmp(msg, "robot, ", 7)) 
		return 0;

	do_robot_cmd(msg);

	return 0;
}

typedef int (* fptr_t) (char *);
fptr_t execute_in(struct cmdtable *, char *, int);

int do_robot_cmd(char *msg) {
	fptr_t f;
	int len;
	char *p;
	
	p = msg += 7;

	for (len = 0; *p && *p != '\n'; p++, len++) 
		;

	if (!(f = execute_in(commands, msg, len))) {
		send_msg("Are you calling me? I don't know what's your meaning. \n", "robot");

		return 0;
	}

	return f(msg);
}


