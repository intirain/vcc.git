/* vcc/plugins/hello/hello.c 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <stdio.h>
#include <string.h>

#include <vcc/vcc.h>
#include <vcc/pretty.h>
#include <vcc/plugin.h>
#include <vcc/version.h>


int do_cmd_hello(int argc, char **argv);

struct cmdtable hello_cmdtab[] = {
	{ "-hello", do_cmd_hello, "Greet you. " }, 

	{ NULL, NULL, NULL }
};

struct plugin plg_hello = {
	.state = 0, 
	.name = "hello", 
	.descr = "Greet you. ", 
	.cmdtab = hello_cmdtab
};

#define this_plg (&plg_hello)


int do_cmd_hello(int argc, char **argv) {
	(void) argv;

	plg_check_enabled();

	if (argc != 1) {
		fprintf(stderr, _("usage: -hello\n"));

		return 1;
	}

	printf("hello, world\n");

	return 0;
}


