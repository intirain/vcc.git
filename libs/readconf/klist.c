/* readconf/klist.c 
 * 
 * This file is part of readconf. 
 * 
 * readconf is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * readconf is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with readconf. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <readconf/klist.h>


struct rc_klist_node *rc_klist_find_value(struct rc_klist_node *head, __uint32_t value) {
	struct rc_klist_node *t;

	for (t = head; t; t = t->next) 
		if (t->value == value) 
			return t;

	return NULL;
}


void rc_klist_init(struct rc_klist_node *head) {
	head->next = head;
	head->prev = head;
	head->value = 0;
	head->payload = NULL;

	return;
}


void rc_klist_add(struct rc_klist_node *head, struct rc_klist_node *new) {
	/* head->prev: the last. */
	new->prev = head->prev;
	new->next = head;

	/* head->prev->next: (was) the head */
	head->prev->next = new;
	head->prev = new;

	return;
}


void rc_klist_del(struct rc_klist_node *head, struct rc_klist_node *will_del) {
	(void) head;

	will_del->prev->next = will_del->next;
	will_del->next->prev = will_del->prev;

	return;
}


