/* readconf/include/readconf.h 
 * 
 * This file is part of readconf. 
 * 
 * readconf is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * readconf is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with readconf. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __READCONF_H
#define __READCONF_H

#include <readconf/port.h>
#include <readconf/do_op_set.h>


#define READCONF_LINE_SPLIT 	'\n'

/* also the size we will allocate for a buffer */
#define READCONF_MAXLINESIZE 	128

#define READCONF_COMMENT 	'#'
#define READCONF_EQUAL 		'='


#define READCONF_WHITESPACE 	" \t\0"



struct readconf_op {
	char *op;
	int oplen;

	int (* do_op) (void);
};


/* in that readconf.c */

extern struct readconf_op ops[];
extern char *line_buffer;
extern int line_no;


/* and here are readconf apis */

int readconf_init(void);

void *readconf_find_name_any(char *name, int *out_type);
struct readconf_strconf *readconf_find_strname(char *name);
struct readconf_uintconf *readconf_find_uintname(char *name);


int do_op_set(void);

char *skip_space(char *);
int len_of(char *);

#endif

