/* readconf/include/port.h
 * 
 * This file is part of readconf. 
 * 
 * readconf is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * readconf is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with readconf. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __PORT_H
#define __PORT_H

/* you can port to another system that unsupports posix, or 
 * port it to a kernel very easily, by using this h. :-) */


/* and here, are the default options on the posix systems.
 * if you want to port, you must implement your own strncmp and strncpy */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>


extern int conf_fd;

#define readconf_puts(s) 		write(2, s, strlen(s))
#define readconf_error(s) 		readconf_puts("error: " s)
#define readconf_errorf(errmsg) 	fprintf(stderr, "libreadconf: " errmsg \
						": \n\t%06d\t\t%s\n", \
						line_no, line_buffer)
#define readconf_printf(s, ...) 	fprintf(stderr, s, ##__VA_ARGS__)


/* this should return -1 if no data left. */

#define readconf_getbyte() 	({	\
	char __v;			\
	read(conf_fd, &__v, 1);		\
	__v;				\
})

/* we assumes these never fail. */

#define readconf_putbyte(b) 	write(conf_fd, &b, 1)

#define readconf_malloc(s) 	malloc(s)
#define readconf_free(s) 	free(s), s = NULL


#endif


