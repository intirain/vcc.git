/* readconf/include/klist.h 
 * 
 * This file is part of readconf. 
 * 
 * readconf is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * readconf is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with readconf. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __KLIST_H
#define __KLIST_H

#include <readconf/port.h>


struct rc_klist_node {
	__uint64_t value;
	void *payload;

	struct rc_klist_node *next, *prev;
};



struct rc_klist_node *rc_klist_find_value(struct rc_klist_node *head, __uint32_t value);

void rc_klist_init(struct rc_klist_node *head);

void rc_klist_add(struct rc_klist_node *head, struct rc_klist_node *new);
void rc_klist_del(struct rc_klist_node *head, struct rc_klist_node *will_del);


#endif

