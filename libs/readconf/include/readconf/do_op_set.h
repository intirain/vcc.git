/* readconf/include/do_op_set.h 
 * 
 * This file is part of readconf. 
 * 
 * readconf is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * readconf is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with readconf. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __DO_OP_SET_H
#define __DO_OP_SET_H

#include <readconf/port.h>
#include <readconf/klist.h>


#define SET_TYPE_INVALID	0
#define SET_TYPE_UINT 		1
#define SET_TYPE_STR 		2


struct readconf_strconf {
	char *conf_key;
	int conf_key_len;

	char *conf_value;
	int conf_value_len;
};


struct readconf_uintconf {
	char *conf_key;
	int conf_key_len;

	__uint64_t conf_value;
};

#define NODETYPE_INVALID 	0
#define NODETYPE_STR		1
#define NODETYPE_UINT 		2


extern struct rc_klist_node name_list;

int show_names(void);

#endif


