/* vcc/intiauth.c 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <vcc/vcc.h>
#include <vcc/plugin.h>
#include <vcc/version.h>
#include <vcc/interfaces.h>


int ia_login(char *session);

int do_intiauth(void) {
	char auth_session[65];
	int fdt, fdu;

	if ((fdt = open("/var/run/intiauth-session", O_RDONLY)) < 0) 
		return 0;

	if ((fdu = open("/var/run/intiauth-user", O_RDONLY)) < 0) {
		close(fdt);

		return 0;
	}

	memset(auth_session, 0, 65);

	if (unlikely(read(fdt, auth_session, 64) != 64)) {
		fprintf(stderr, "/var/run/intiauth-session: wrong file format: too short. \n");
		close(fdt);

		return 0;
	}

	read(fdu, usrname, USRNAME_SIZE);

	close(fdt);
	close(fdu);

	printf("intiauth: %s %s\n", usrname, auth_session);

	ia_login(auth_session);

	return 1;
}

