/* vcc/pretty.c 
 * 
 * This file is part of vcc. 
 * 
 * vcc is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vcc is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vcc. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <stdio.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/times.h>

#include <vcc/vcc.h>
#include <vcc/version.h>
#include <vcc/pretty.h>
#include <vcc/plugin.h>
#include <vcc/interfaces.h>
#include <readconf/readconf.h>


struct theme theme_def = {
	.usrname = {
		.fg = FG_LIGHTBLUE, 
		.bg = BG_BLACK, 
		.mode = MODE_LINE
	}, 
	
	.sysusername = {
		.fg = FG_RED, 
		.bg = BG_BLACK, 
		.mode = MODE_LINE
	}, 

	.msg = {
		.fg = FG_YELLOW, 
		.bg = BG_BLACK, 
		.mode = MODE_HIGHLIGHT
	}, 

	.sysmsg = {
		.fg = FG_GREEN, 
		.bg = BG_BLACK, 
		.mode = MODE_HIGHLIGHT
	}, 

	.sid = {
		.fg = FG_RED, 
		.bg = BG_BLACK ,
		.mode = MODE_HIGHLIGHT
	}, 

	.lvl = {
		.fg = FG_YELLOW, 
		.bg = BG_BLACK, 
		.mode = MODE_LINE
	}, 

	.time = {
		.fg = FG_YELLOW, 
		.bg = BG_BLACK, 
		.mode = MODE_HIGHLIGHT
	}, 

	.relay = {
		.fg = FG_BLUE, 
		.bg = BG_BLACK, 
		.mode = MODE_HIGHLIGHT
	}, 
	
	.errmsg = {
		.fg = FG_RED, 
		.bg = BG_BLACK, 
		.mode = MODE_HIGHLIGHT
	}, 

	.helpcmd = {
		.fg = FG_LIGHTBLUE, 
		.bg = BG_BLACK, 
		.mode = MODE_HIGHLIGHT
	}, 

	.helpmsg = {
		.fg = FG_WHITE, 
		.bg = BG_BLACK, 
		.mode = MODE_LINE
	}, 

	.cqd = {
		.fg = FG_BLACK, 
		.bg = BG_RED, 
		.mode = MODE_BLINK
	}
};

struct theme *current_theme = &theme_def;
int self_level;
int conf_fd;

#include "gnr_theme_macros.s"


int open_pretty_conf(void) {
	char 	*home;
	char 	path[PATH_MAX];

	if (unlikely(!(home = getenv("HOME")))) 
		goto abandon;
	
	sprintf(path, "%s/.vcc-theme", home);

	if ((conf_fd = open(path, O_RDONLY)) < 0) 
		goto abandon;

	printf("reading theme ...");
	return 0;

abandon:
	fprintf(stderr, "using default theme. \n");
	return 1;
}

int nr_ready = 0;

int parse_theme(char *name, struct color *p) {
	struct readconf_uintconf *conf;
	char key[32];

	sprintf(key, "%s_mode", name);

#define tryload \
	if (!(conf = readconf_find_uintname(key))) { \
		fprintf(stderr, "cannot load theme %s\n", name); \
		return 1; \
	}

	tryload;
	p->mode = conf->conf_value;

	memset(key, 0, 32);
	sprintf(key, "%s_fg", name);

	tryload;
	p->fg = conf->conf_value;

	memset(key, 0, 32);
	sprintf(key, "%s_bg", name);

	tryload;
	p->bg = conf->conf_value;

	return 0;
}

int load_themes(void) {
#include "gnr_load_themes.s"

	return 0;
}


int pretty_init(void) {
	if (open_pretty_conf()) 
		return 0;

	readconf_init();
	load_themes();

	close(conf_fd);

	return 0;
}



int pretty_new_msg(char *usrname, char *buf, int sid, int flags) {
	time_t time_now;
	struct tm *tm_now;
	char *sname;

	sname = getsname(sid);

	ansi_csi_st(stdout, MODE_HIGHLIGHT, FG_BLACK, BG_BLACK);

	setcolor(stdout, curr_time);
	time_now = time(NULL);
	tm_now = localtime(&time_now);
	printf("\r[%02d:%02d] ", tm_now->tm_hour, tm_now->tm_min);

	setcolor(stdout, curr_sid);

	printf("#%s ", sname);

	if (flags & MSG_NEW_RELAY) {
		setcolor(stdout, curr_relay);
		printf("[relay] ");

		if (flags & MSG_NEW_ONLY_VISIBLE) 
			printf("[to-u-only] ");

		else 
			printf("[broadcast] ");
	}

	if (flags & MSG_ENCRYPTED) {
		setcolor(stdout, curr_time);
		printf("[encrypted] ");
	}

	if (unlikely(!strcmp(usrname, "system"))) {
		setcolor(stdout, curr_sysusername);
		printf("system");
	}

	else {
		setcolor(stdout, curr_usrname);
		printf("%s", usrname);
	}

	ansi_csi_ed(stdout);

	printf("@: ");

	if (unlikely(!strcmp(usrname, "system"))) {
		setcolor(stdout, curr_sysmsg);

		printf("%s\n", buf);
	}

	else {
		setcolor(stdout, curr_msg);
		printf("%s\n", buf);
	}

	ansi_csi_ed(stdout);

	return 0;
}

int pretty_prompt(char *usrname) {
	char *sname;

	sname = getsname(current_sid);
	ansi_csi_st(stderr, MODE_HIGHLIGHT, FG_BLACK, BG_BLACK);

	setcolor(stderr, curr_sid);
	fprintf(stderr, "#%s ", sname);

	setcolor(stderr, curr_lvl);
	fprintf(stderr, "lvl%d ", self_level);

	setcolor(stderr, curr_usrname);
	fprintf(stderr, "%s", usrname);
	
	ansi_csi_ed(stderr);
	fprintf(stderr, "$: ");

	return 0;
}


int pretty_unknown_cmd(char *s) {
	fprintf(stderr, _("unknown command: "));

	setcolor(stderr, curr_errmsg);
	fputs(s, stderr);
	fputc('\n', stderr);
	ansi_csi_ed(stderr);

	return 0;
}


int pretty_cmd_help(char *cmdname, char *descr) {
	setcolor(stdout, curr_helpcmd);
	printf("%s:", cmdname);

	setcolor(stdout, curr_helpmsg);
	printf("\t%s\n", _(descr));

	ansi_csi_ed(stdout);

	return 0;
}


int pretty_cqd(char *username, char *msg) {
	printf("\n");

	setcolor(stdout, curr_cqd);
	printf("CQD");

	ansi_csi_ed(stdout);

	printf(_(" %s sent %s"), username, msg);

	return 0;
}


